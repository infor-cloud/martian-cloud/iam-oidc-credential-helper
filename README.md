# IAM OIDC Credential Helper

The IAM OIDC Credential Helper uses the martian cloud credential helper protocol to provide support for getting a token from the iam-oidc-provider.

## Security

If you've discovered a security vulnerability in this project, please let us know by creating a **confidential** issue in this project.

## Statement of support

Please submit any bugs or feature requests for this project.  Of course, MR's are even better.  :)

## License

This project is distributed under [Mozilla Public License v2.0](https://www.mozilla.org/en-US/MPL/2.0/).
