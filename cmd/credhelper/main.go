// main package
package main

import (
	"context"
	"crypto/sha256"
	"encoding/json"
	"flag"
	"fmt"
	"io"
	"log"
	"net/http"
	"strings"
	"time"

	v4 "github.com/aws/aws-sdk-go-v2/aws/signer/v4"
	awsconfig "github.com/aws/aws-sdk-go-v2/config"
)

type iamOidcTokenResponse struct {
	Token     string `json:"access_token"`
	ExpiresIn int    `json:"expires_in"`
}

type credentialHelperResponse struct {
	Token string `json:"token"`
}

func main() {
	ctx := context.Background()

	regionPtr := flag.String("region", "us-east-1", "AWS Region")
	endpoint := flag.String("endpoint", "", "Service Endpoint")
	audience := flag.String("aud", "", "Audience")

	flag.Parse()

	if *endpoint == "" {
		log.Fatal("endpoint flag is required")
	}

	if *audience == "" {
		log.Fatal("aud flag is required")
	}

	awsCfg, err := awsconfig.LoadDefaultConfig(ctx, awsconfig.WithRegion(*regionPtr))
	if err != nil {
		log.Fatalf("failed to initialize aws config: %v", err)
	}

	creds, err := awsCfg.Credentials.Retrieve(ctx)
	if err != nil {
		log.Fatalf("failed to retriev aws credentials: %v", err)
	}

	signer := v4.NewSigner()

	body := fmt.Sprintf("aud=%s", *audience)

	req, err := http.NewRequestWithContext(ctx, http.MethodPost, fmt.Sprintf("%s/token", *endpoint), strings.NewReader(body))
	if err != nil {
		log.Fatalf("failed to create http request: %v", err)
	}

	// hash the request body
	hash := sha256.Sum256([]byte(body))
	// hex value used in the signature
	hexHash := fmt.Sprintf("%x", hash)

	err = signer.SignHTTP(ctx, creds, req, hexHash, "execute-api", awsCfg.Region, time.Now())
	if err != nil {
		log.Fatalf("failed to create aws v4 signature: %v", err)
	}

	// create http client with timeout
	client := &http.Client{Timeout: 30 * time.Second}
	resp, err := client.Do(req)
	if err != nil {
		log.Fatalf("http request failed: %v", err)
	}

	respBody, err := io.ReadAll(resp.Body)
	if err != nil {
		log.Fatalf("failed to read http response body: %v", err)
	}

	if (resp.StatusCode != http.StatusCreated) && (resp.StatusCode != http.StatusOK) {
		log.Fatalf("http request failed with status code %s: %s", resp.Status, respBody)
	}

	var tokenResp iamOidcTokenResponse
	if err = json.Unmarshal(respBody, &tokenResp); err != nil {
		log.Fatalf("failed to parse response body: %v", err)
	}

	output, err := json.Marshal(credentialHelperResponse{Token: tokenResp.Token})
	if err != nil {
		log.Fatalf("failed to create token response: %v", err)
	}

	// output response to stdout
	fmt.Println(string(output))
}
